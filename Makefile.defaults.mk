# This file contains the default variables used in the Makefile
# If you want to change them, duplicate the file, name it "Makefile.defaults.custom.mk" and make the changes you want

# Logging levels described by RFC 5424. https://github.com/Seldaek/monolog/blob/main/doc/01-usage.md#log-levels
BASH_EXT_LOG_LEVEL_EMERGENCY=600 # Emergency: system is unusable.
BASH_EXT_LOG_LEVEL_ALERT=550     # Action must be taken immediately. Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.
BASH_EXT_LOG_LEVEL_CRITICAL=500  # Critical conditions. Example: Application component unavailable, unexpected exception.
BASH_EXT_LOG_LEVEL_ERROR=400     # Runtime errors that do not require immediate action but should typically be logged and monitored.
BASH_EXT_LOG_LEVEL_WARNING=300   # Exceptional occurrences that are not errors. Examples: Use of deprecated APIs, poor use of an API, undesirable things that are not necessarily wrong.
BASH_EXT_LOG_LEVEL_NOTICE=250    # Normal but significant events.
BASH_EXT_LOG_LEVEL_INFO=200      # Interesting events. Examples: User logs in, SQL logs.
BASH_EXT_LOG_LEVEL_DEBUG=100     # Detailed debug information.
