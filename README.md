# Bash Deployer

A project for making simple deployments to remote servers.

It can:
- Deploy to an empty folder, on  one or several servers simultaneously;
- Roll back from a release, automatically removing the faulty release, while also preventing the removal of all releases;
- Keep only a configurable number of releases, to which we can roll back to, removing the oldest outdated releases;
- Execute custom instructions at specific points of the deployment process;
- Automatically roll back a release deployment if something goes wrong.

### Deployment

The actions performed when deploying are as follows:
1. Transfer the current local release to the remote server(s);
   1. Copy the remote current release to the new release folder (if it exists);
   2. rsync the (new) local release to the new release remote location(s);
2. Update the 'previous' symlink to the current (old) release.
3. Create or update the 'current' symlink to the newly transferred release.
4. Remove the outdated release(s), beyond the predefined limit.

It can also execute custom instructions:
- before transferring the release to the remote location(s);
- before switching the current release to the newly transferred release;
- after switching the current release to the newly transferred release, on all servers;
- after switching the current release to the newly transferred release, on a single server.

### Roll back

The actions performed when rolling back are as follows:
1. Replace the 'current' release symlinc for the 'previous' release symlink;
2. Create a new 'previous' release symlink, pointing to the release, based on time;
3. Remove the rolled back release.

## How to use

Setup SSH access from the deployer machine to the target machine(s)

Create a file with the list of remote hosts where to deploy the application

### Example deployment

```shell
# supply either TARGET_LIST_STRING or TARGET_LIST_FILE
deployer.sh deploy \
  SSH_KEY_FILE=${ID_RSA_FILE_PATH} \
  TARGET_LIST_STRING="'$(declare -p TARGET_ARRAY)'" \
  TARGET_LIST_FILE=${TARGET_LIST_FILE} \
  DEPLOYMENT_BASE_PATH='/var/www' \
  SOURCE_PATH="./" \
  RELEASES_TO_KEEP=3 \
  ARTIFACT_OUT_PATH='var/ci-out' \
  DELETE_EXTRANEOUS_FILES_FROM_DESTINATION='true' \
  EXCLUSION_PATHS="'.git' 'node_modules'"
```

### Example rollback

```shell
# supply either TARGET_LIST_STRING or TARGET_LIST_FILE
deployer.sh rollback \
  SSH_KEY_FILE=${ID_RSA_FILE_PATH} \
  TARGET_LIST_STRING="'$(declare -p TARGET_ARRAY)'" \
  TARGET_LIST_FILE=${TARGET_LIST_FILE} \
  DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}"
```

### Example usage of deployment with custom actions

For hooking into the deployer and add custom behaviour, we can create functions with specific names,
which will be called during the running of a deployment.

For this special functions to be available to the deployer, you need to create your own deployer,
which will wrap this projects' deployer:

`# my_deployer.sh`
```shell
#!/usr/bin/env bash

before_transfer() {
  # ... ex: build the application
}

before_release_link_update() {
  local NEW_RELEASE_PATH
  local "${@}"

  # ...
}

after_release_link_update_on_all_servers() {
  local NEW_RELEASE_PATH
  local "${@}"

  # ... ex: restart services like supervisor
}

after_release_link_update_on_one_server() {
  local NEW_RELEASE_PATH
  local "${@}"

  # ... ex: run migrations
}

. ./some/path/to/bash-deployer/bin/deployer.sh
```

## Tests

Currently, there are no tests, but I hope to add some at some point.
