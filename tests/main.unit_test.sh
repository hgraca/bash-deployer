#!/usr/bin/env bash

# shellcheck source-path=SCRIPTDIR
DIR_bash_bash_deployer_tests=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
DIR_ROOT="${DIR_bash_bash_deployer_tests}/.."
. "${DIR_ROOT}/src/loader.sh"

unit_test.it_should_deploy_to_an_empty_deployment_path() {
  TEST_PATH="${DIR_TESTS_TMP}/${FUNCNAME[0]}"
  SOURCE="${TEST_PATH}/source"
  TARGET1="${TEST_PATH}/target1"
  TARGET2="${TEST_PATH}/target2"
  mkdir -p "${SOURCE}"
  mkdir -p "${TARGET1}"
  mkdir -p "${TARGET2}"
  TEST_FILE="test-file.txt"
  echo 'aaa' >  "${SOURCE}/${TEST_FILE}"
  TARGET_ARRAY=( "${TARGET1}" "${TARGET2}" )
  DEPLOYMENT_BASE_PATH='my_project'
  RETURN_CODE=0

  "${DIR_ROOT}/bin/deployer.sh"  deploy \
    TARGET_LIST_STRING="'$(declare -p TARGET_ARRAY)'" \
    DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}" \
    SOURCE_PATH="${SOURCE}" \
    ARTIFACT_OUT_PATH="${TEST_PATH}/ci-out"
  RETURN_CODE=$?

  if [ "${RETURN_CODE}" == "0" ] \
    && test.assert_file_exists "${TARGET1}/${DEPLOYMENT_BASE_PATH}/current/${TEST_FILE}" \
    && test.assert_file_exists "${TARGET2}/${DEPLOYMENT_BASE_PATH}/current/${TEST_FILE}" \
    && test.assert_files_have_same_content "${SOURCE}/${TEST_FILE}" "${TARGET1}/${DEPLOYMENT_BASE_PATH}/current/${TEST_FILE}" \
    && test.assert_files_have_same_content "${SOURCE}/${TEST_FILE}" "${TARGET2}/${DEPLOYMENT_BASE_PATH}/current/${TEST_FILE}"; then
    RESULT=0
    test.cleanup_fs "${TEST_PATH}"
  else
    RESULT=1
  fi

  return ${RESULT}
}

unit_test.it_should_deploy_a_new_release_to_an_existing_deployment() {
  TEST_PATH="${DIR_TESTS_TMP}/${FUNCNAME[0]}"
  SOURCE="${TEST_PATH}/source"
  TARGET1="${TEST_PATH}/target1"
  TARGET2="${TEST_PATH}/target2"
  mkdir -p "${SOURCE}"
  mkdir -p "${TARGET1}"
  mkdir -p "${TARGET2}"
  TEST_FILE="test-file.txt"
  echo 'aaa' | tee "${SOURCE}/${TEST_FILE}" "${TEST_PATH}/${TEST_FILE}" > /dev/null
  TARGET_ARRAY=( "${TARGET1}" "${TARGET2}" )
  DEPLOYMENT_BASE_PATH='my_project'
  RETURN_CODE=0
  "${DIR_ROOT}/bin/deployer.sh"  deploy \
    TARGET_LIST_STRING="'$(declare -p TARGET_ARRAY)'" \
    DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}" \
    SOURCE_PATH="${SOURCE}" \
    ARTIFACT_OUT_PATH="${TEST_PATH}/ci-out"
  RETURN_CODE=$?
  PREVIOUS_IN_TARGET_1=$(realpath "${TARGET1}/${DEPLOYMENT_BASE_PATH}/current/${TEST_FILE}")
  PREVIOUS_IN_TARGET_2=$(realpath "${TARGET2}/${DEPLOYMENT_BASE_PATH}/current/${TEST_FILE}")
  echo 'bbb' >  "${SOURCE}/${TEST_FILE}"

  sleep 1s # so that the release path is different
  "${DIR_ROOT}/bin/deployer.sh"  deploy \
    TARGET_LIST_STRING="'$(declare -p TARGET_ARRAY)'" \
    DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}" \
    SOURCE_PATH="${SOURCE}" \
    ARTIFACT_OUT_PATH="${TEST_PATH}/ci-out"
  RETURN_CODE=$?

  if [ "${RETURN_CODE}" == "0" ] \
    && test.assert_file_exists "${TARGET1}/${DEPLOYMENT_BASE_PATH}/current/${TEST_FILE}" \
    && test.assert_file_exists "${TARGET1}/${DEPLOYMENT_BASE_PATH}/previous/${TEST_FILE}" \
    && test.assert_files_have_same_content "${SOURCE}/${TEST_FILE}" "${TARGET1}/${DEPLOYMENT_BASE_PATH}/current/${TEST_FILE}" \
    && test.assert_files_have_same_content "${TEST_PATH}/${TEST_FILE}" "${PREVIOUS_IN_TARGET_1}" \
    && test.assert_file_exists "${TARGET2}/${DEPLOYMENT_BASE_PATH}/current/${TEST_FILE}" \
    && test.assert_file_exists "${TARGET2}/${DEPLOYMENT_BASE_PATH}/previous/${TEST_FILE}" \
    && test.assert_files_have_same_content "${SOURCE}/${TEST_FILE}" "${TARGET2}/${DEPLOYMENT_BASE_PATH}/current/${TEST_FILE}" \
    && test.assert_files_have_same_content "${TEST_PATH}/${TEST_FILE}" "${PREVIOUS_IN_TARGET_2}"; then
    RESULT=0
    test.cleanup_fs "${TEST_PATH}"
  else
    RESULT=1
  fi

  return ${RESULT}
}

unit_test.it_should_remove_outdated_releases() {
  TEST_PATH="${DIR_TESTS_TMP}/${FUNCNAME[0]}"
  SOURCE="${TEST_PATH}/source"
  TARGET1="${TEST_PATH}/target1"
  TARGET2="${TEST_PATH}/target2"
  mkdir -p "${SOURCE}"
  mkdir -p "${TARGET1}"
  mkdir -p "${TARGET2}"
  TEST_FILE="test-file.txt"
  echo 'aaa' | tee "${SOURCE}/${TEST_FILE}" "${TEST_PATH}/${TEST_FILE}" > /dev/null
  TARGET_ARRAY=( "${TARGET1}" "${TARGET2}" )
  DEPLOYMENT_BASE_PATH='my_project'
  RETURN_CODE=0
  RELEASES_TO_KEEP=3

  for (( I = 1; I <= RELEASES_TO_KEEP+1; I++ )); do
    if [ "${I}" -gt 1 ]; then
      sleep 1s # so that the release path is different
    fi
    "${DIR_ROOT}/bin/deployer.sh"  deploy \
      TARGET_LIST_STRING="'$(declare -p TARGET_ARRAY)'" \
      DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}" \
      SOURCE_PATH="${SOURCE}" \
      ARTIFACT_OUT_PATH="${TEST_PATH}/ci-out" \
      RELEASES_TO_KEEP=${RELEASES_TO_KEEP}
  done

  RELEASES_COUNT=$(dir.count_dir "${TARGET1}/${DEPLOYMENT_BASE_PATH}/releases")

  if [ "${RELEASES_COUNT}" == "${RELEASES_TO_KEEP}" ]; then
    RESULT=0
    test.cleanup_fs "${TEST_PATH}"
  else
    RESULT=1
    stdout.log_error "The release count should be ${RELEASES_TO_KEEP} but it's ${RELEASES_COUNT}."
  fi

  return ${RESULT}
}

unit_test.it_should_rollback() {
  TEST_PATH="${DIR_TESTS_TMP}/${FUNCNAME[0]}"
  SOURCE="${TEST_PATH}/source"
  TARGET1="${TEST_PATH}/target1"
  mkdir -p "${SOURCE}"
  mkdir -p "${TARGET1}"
  TEST_FILE="test-file.txt"
  echo 'aaa' > "${SOURCE}/${TEST_FILE}"
  TARGET_ARRAY=( "${TARGET1}" )
  DEPLOYMENT_BASE_PATH='my_project'
  RETURN_CODE=0
  INITIAL_RELEASES=3
  for (( I = 1; I <= INITIAL_RELEASES; I++ )); do
    if [ "${I}" -gt 1 ]; then
      sleep 1s # so that the release path is different
    fi
    "${DIR_ROOT}/bin/deployer.sh"  deploy \
      TARGET_LIST_STRING="'$(declare -p TARGET_ARRAY)'" \
      DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}" \
      SOURCE_PATH="${SOURCE}" \
      ARTIFACT_OUT_PATH="${TEST_PATH}/ci-out"
  done

  # shellcheck disable=SC2034
  eval declare -a INITIAL_RELEASES_LIST="$( get_releases_list RELEASES_BASE_PATH="${TARGET1}/${DEPLOYMENT_BASE_PATH}/releases" )"
  RELEASE_TO_REMOVE="$(array.max INITIAL_RELEASES_LIST)"

  "${DIR_ROOT}/bin/deployer.sh"  rollback \
    TARGET_LIST_STRING="'$(declare -p TARGET_ARRAY)'" \
    DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}"

  # shellcheck disable=SC2034
  eval declare -a FINAL_RELEASES_LIST="$( get_releases_list RELEASES_BASE_PATH="${TARGET1}/${DEPLOYMENT_BASE_PATH}/releases" )"
  RELEASE_REMOVED="$(array.diff INITIAL_RELEASES_LIST FINAL_RELEASES_LIST)"
  RELEASES_COUNT=$(dir.count_dir "${TARGET1}/${DEPLOYMENT_BASE_PATH}/releases")
  if [ "${RELEASES_COUNT}" -eq "$((INITIAL_RELEASES - 1))" ] \
    && [ "${RELEASE_TO_REMOVE}" == "${RELEASE_REMOVED}" ]; then
    RESULT=0
    test.cleanup_fs "${TEST_PATH}"
  else
    RESULT=1
    if [ "$((INITIAL_RELEASES - 1))" -ne "${RELEASES_COUNT}" ]; then
      stdout.log_error "The release count should be $((INITIAL_RELEASES - 1)) but it's ${RELEASES_COUNT}."
    else
      stdout.log_error "It should have removed '${RELEASE_TO_REMOVE}' from '${INITIAL_RELEASES_LIST[*]}' but it removed '${RELEASE_REMOVED}'."
    fi
  fi

  return ${RESULT}
}

unit_test.it_should_not_rollback_if_theres_only_one_release() {
  TEST_PATH="${DIR_TESTS_TMP}/${FUNCNAME[0]}"
  SOURCE="${TEST_PATH}/source"
  TARGET1="${TEST_PATH}/target1"
  mkdir -p "${SOURCE}"
  mkdir -p "${TARGET1}"
  TEST_FILE="test-file.txt"
  echo 'aaa' > "${SOURCE}/${TEST_FILE}"
  TARGET_ARRAY=( "${TARGET1}" )
  DEPLOYMENT_BASE_PATH='my_project'
  RETURN_CODE=0
  "${DIR_ROOT}/bin/deployer.sh"  deploy \
    TARGET_LIST_STRING="'$(declare -p TARGET_ARRAY)'" \
    DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}" \
    SOURCE_PATH="${SOURCE}" \
    ARTIFACT_OUT_PATH="${TEST_PATH}/ci-out"

  "${DIR_ROOT}/bin/deployer.sh"  rollback \
    TARGET_LIST_STRING="'$(declare -p TARGET_ARRAY)'" \
    DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}"
  RETURN_CODE=$?

  RELEASES_COUNT=$(dir.count_dir "${TARGET1}/${DEPLOYMENT_BASE_PATH}/releases")
  if [ "${RELEASES_COUNT}" -eq "1" ] && [ "${RETURN_CODE}" -ne '0' ]; then
    RESULT=0
    test.cleanup_fs "${TEST_PATH}"
  else
    RESULT=1
    stdout.log_error "The release count should be 1 but it's ${RELEASES_COUNT}."
  fi

  return ${RESULT}
}
