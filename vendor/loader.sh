#!/usr/bin/env bash

# shellcheck source-path=SCRIPTDIR
DIR_bash_deployer_vendor=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
. "${DIR_bash_deployer_vendor}/hgraca/bash-extension/src/loader.sh"
