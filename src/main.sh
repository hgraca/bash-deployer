#!/usr/bin/env bash

# shellcheck source-path=SCRIPTDIR
DIR_bash_deployer_src=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
. "${DIR_bash_deployer_src}/loader.sh"

ACTION=${1:-'deploy'}
ARGS=( "${@}" )
unset "ARGS[0]"
RETURN_CODE=0

#=======================================================================================================================
# FUNCTIONS
#=======================================================================================================================

help() {
  echo "Usage: ${0} <ACTION> [-h]"
  echo "Actions:"
  echo ""
  echo "         deployer.sh deploy \
                   SSH_KEY_FILE=\"\${ID_RSA_FILE_PATH}\" \
                   TARGET_LIST_STRING='declare -a TARGET_ARRAY=([0]=\"user@8.8.8.8:/var/www\" [1]=\"/var/www\")' \ # get this by doing: \"\$(declare -p TARGET_ARRAY)\"
                   TARGET_LIST_FILE=\"\${TARGET_LIST_FILE}\" \ # Either TARGET_LIST_STRING or TARGET_LIST_FILE must be provided
                   DEPLOYMENT_BASE_PATH='./my_project' \ # This is relative to the target
                   SOURCE_PATH='./' \
                   RELEASES_TO_KEEP=3 \
                   ARTIFACT_OUT_PATH='var/ci-out' \
                   DELETE_EXTRANEOUS_FILES_FROM_DESTINATION='true' \
                   EXCLUSION_PATHS=\"'.git' 'node_modules'\""
  echo ""
  echo "         deployer.sh rollback \
                   SSH_KEY_FILE=\"\${ID_RSA_FILE_PATH}\" \
                   TARGET_LIST_STRING='declare -a TARGET_ARRAY=([0]=\"user@8.8.8.8:/var/www\" [1]=\"/var/www\")' \ # get this by doing: \"\$(declare -p TARGET_ARRAY)\"
                   TARGET_LIST_FILE=\"\${TARGET_LIST_FILE}\" \
                   DEPLOYMENT_BASE_PATH='./my_project'  # This is relative to the target "
}

validate_host() {
  ssh -V &>/dev/null
  if [ "$?" != "0" ]; then
    stdout.log_error "This script needs the ssh client."
    exit 1
  fi
  rsync --version &>/dev/null
  if [ "$?" != "0" ]; then
    stdout.log_error "This script needs rsync."
    exit 1
  fi
  if [ "${BASH_VERSINFO[0]}${BASH_VERSINFO[1]}" -lt "51" ]; then
    stdout.log_error "This script needs Bash version 5.1 or above. Current version: ${BASH_VERSINFO[0]}.${BASH_VERSINFO[1]}"
    exit 1
  fi
}

#=======================================================================================================================
# MAIN
#=======================================================================================================================

validate_host
RETURN_CODE=$?
if [ "${RETURN_CODE}" -ne "0" ]; then
  stdout.log_error "Exit code: ${RETURN_CODE}"
  exit "${RETURN_CODE}"
fi

case ${ACTION} in
  # --------------------------------------------------------------------------------------------------------------------
  'deploy')
    SSH_KEY_FILE=''
    TARGET_LIST_STRING=''
    TARGET_LIST_FILE=''
    DEPLOYMENT_BASE_PATH='./'
    SOURCE_PATH="./"
    ARTIFACT_OUT_PATH='var/ci-out'
    RELEASES_TO_KEEP=3
    DELETE_EXTRANEOUS_FILES_FROM_DESTINATION='true'
    EXCLUSION_PATHS="'.git' 'node_modules'"
    eval "${ARGS[@]}"

    if [ -e "${TARGET_LIST_FILE}" ]; then
      readarray -t TARGET_LIST < "${TARGET_LIST_FILE}"
    elif [ "${TARGET_LIST_STRING}" != '' ]; then
      # eval string into a new associative array
      eval "declare -A TARGET_LIST=${TARGET_LIST_STRING#*=}"
    else
      stdout.log_error "No target list was given."
      exit 1
    fi

    mkdir -p "${ARTIFACT_OUT_PATH}"
    if file.exists "${SSH_KEY_FILE}"; then
      chmod 600 "${SSH_KEY_FILE}"
    fi

    remote.deploy_or_exit SSH_KEY_FILE="${SSH_KEY_FILE}" \
      TARGET_LIST_STRING="$(declare -p TARGET_LIST)" \
      DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}" \
      SOURCE_PATH="${SOURCE_PATH}" \
      RELEASES_TO_KEEP="${RELEASES_TO_KEEP}" \
      DELETE_EXTRANEOUS_FILES_FROM_DESTINATION="${DELETE_EXTRANEOUS_FILES_FROM_DESTINATION}" \
      EXCLUSION_PATHS="${EXCLUSION_PATHS}"
    RETURN_CODE=$?

    stdout.log_info "Check the generated artifacts to see all output."
    ;;
  # --------------------------------------------------------------------------------------------------------------------
  'rollback')
    SSH_KEY_FILE='' TARGET_LIST_FILE='' DEPLOYMENT_BASE_PATH='./' ARTIFACT_OUT_PATH='var/ci-out'
    eval "${ARGS[@]}"

    if [ -e "${TARGET_LIST_FILE}" ]; then
      readarray -t TARGET_LIST < "${TARGET_LIST_FILE}"
    elif [ "${TARGET_LIST_STRING}" != '' ]; then
      # eval string into a new associative array
      eval "declare -A TARGET_LIST=${TARGET_LIST_STRING#*=}"
    else
      stdout.log_error "No target list was given."
      exit 1
    fi

    mkdir -p "${ARTIFACT_OUT_PATH}"
    if file.exists "${SSH_KEY_FILE}"; then
      chmod 600 "${SSH_KEY_FILE}"
    fi

    remote.rollback_or_exit SSH_KEY_FILE="${SSH_KEY_FILE}" \
      TARGET_LIST_STRING="$(declare -p TARGET_LIST)" \
      DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}"
    RETURN_CODE=$?

    stdout.log_info "Check the generated artifacts to see all output."
    ;;
  # --------------------------------------------------------------------------------------------------------------------
  :)
    echo "Invalid action: ${ACTION}"
    help
    exit 4
    ;;
  # --------------------------------------------------------------------------------------------------------------------
esac

echo ''
echo ''

exit $RETURN_CODE
