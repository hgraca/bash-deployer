#!/usr/bin/env bash

# shellcheck source-path=SCRIPTDIR
DIR_bash_deployer_src=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
. "${DIR_bash_deployer_src}/../vendor/loader.sh"
. "${DIR_bash_deployer_src}/functions/deploy.sh"
. "${DIR_bash_deployer_src}/functions/releases.sh"
. "${DIR_bash_deployer_src}/functions/remove_releases.sh"
. "${DIR_bash_deployer_src}/functions/rollback.sh"
