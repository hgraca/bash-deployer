#!/usr/bin/env bash

rollback() {
  local RELEASES_BASE_PATH CURRENT_RELEASE_LINK PREVIOUS_RELEASE_LINK
  local "${@}"

  OLD_CURRENT_RELEASE_PATH="$(readlink "${CURRENT_RELEASE_LINK}")" # This needs to be done BEFORE updating the current release link
  OLD_CURRENT_RELEASE_PATH="${RELEASES_BASE_PATH}/$(basename "$(readlink "${CURRENT_RELEASE_LINK}")")"

  eval declare -a RELEASES="$( get_releases_list RELEASES_BASE_PATH="${RELEASES_BASE_PATH}" )"
  stdout.log_debug "RELEASES=${RELEASES[*]}"
  stdout.log_debug "RELEASES=$(declare -p RELEASES)"

  if [ "${#RELEASES[@]}" -le "1" ]; then
    stdout.log_error "There is only one release, or no release at all."
    exit 1
  fi

  PREVIOUS_RELEASE_PATH="$(readlink "${PREVIOUS_RELEASE_LINK}")"
  stdout.log_debug "PREVIOUS_RELEASE_PATH=$PREVIOUS_RELEASE_PATH"

  PREVIOUS_RELEASE=$(basename "${PREVIOUS_RELEASE_PATH}")
  stdout.log_debug "PREVIOUS_RELEASE=$PREVIOUS_RELEASE"

  PREVIOUS_RELEASE_INDEX=$(array.find_index "${PREVIOUS_RELEASE}" RELEASES)
  stdout.log_debug "PREVIOUS_RELEASE_INDEX=$PREVIOUS_RELEASE_INDEX"

  # 'mv' is an atomic operation, while 'ln -sf' is not. @see https://gist.github.com/hussfelt/3931118#motivation
  mv -T "${PREVIOUS_RELEASE_LINK}" "${CURRENT_RELEASE_LINK}"

  if [ "${PREVIOUS_RELEASE_INDEX}" -ge "$(( $(array.count RELEASES)-1 ))" ]; then
    rm -f "${PREVIOUS_RELEASE_LINK}"
  else
    NEW_PREVIOUS_RELEASE_INDEX=$((PREVIOUS_RELEASE_INDEX+1))
    stdout.log_debug "NEW_PREVIOUS_RELEASE_INDEX=$NEW_PREVIOUS_RELEASE_INDEX"

    NEW_PREVIOUS_RELEASE=${RELEASES[$NEW_PREVIOUS_RELEASE_INDEX]}
    stdout.log_debug "NEW_PREVIOUS_RELEASE=$NEW_PREVIOUS_RELEASE"

    NEW_PREVIOUS_RELEASE_PATH=${RELEASES_BASE_PATH}/${NEW_PREVIOUS_RELEASE}
    stdout.log_debug "NEW_PREVIOUS_RELEASE_PATH=$NEW_PREVIOUS_RELEASE_PATH"

    ln -s "$(realpath --relative-to="$(dirname "${PREVIOUS_RELEASE_LINK}")" "${NEW_PREVIOUS_RELEASE_PATH}")" "${PREVIOUS_RELEASE_LINK}"
  fi

  stdout.log_debug "remove_release RELEASE_PATH=\"${OLD_CURRENT_RELEASE_PATH}\""
  remove_release RELEASE_PATH="${OLD_CURRENT_RELEASE_PATH}"
}

remote.rollback() {
  local SSH_KEY_FILE TARGET_LIST_STRING DEPLOYMENT_BASE_PATH
  local "${@}"

  RELEASES_BASE_PATH=$(get_releases_base_path "${DEPLOYMENT_BASE_PATH}")
  CURRENT_RELEASE_LINK=$(get_current_release_link "${DEPLOYMENT_BASE_PATH}")
  PREVIOUS_RELEASE_LINK=$(get_previous_release_link "${DEPLOYMENT_BASE_PATH}")

  stdout.log_info "Rolling back on RELEASES_BASE_PATH='${RELEASES_BASE_PATH}' CURRENT_RELEASE_LINK='${CURRENT_RELEASE_LINK}' PREVIOUS_RELEASE_LINK='${PREVIOUS_RELEASE_LINK}' ..."

  parallel.ssh SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    LOG_FILE="${ARTIFACT_OUT_PATH}/${FUNCNAME[0]}" \
    SCRIPT="rollback RELEASES_BASE_PATH='${RELEASES_BASE_PATH}' CURRENT_RELEASE_LINK='${CURRENT_RELEASE_LINK}' PREVIOUS_RELEASE_LINK='${PREVIOUS_RELEASE_LINK}'"

  RETURN_CODE=$?
  if error.is_error $RETURN_CODE; then
    return ${RETURN_CODE}
  fi

  stdout.log_info "Done!"
}

# This is to be used when there are errors in deployment, BEFORE updating the current release link
# in which case we only need to remove the aborted release folder
remote.rollback_if_error() {
  local RETURN_CODE="0" SSH_KEY_FILE TARGET_LIST_STRING DEPLOYMENT_BASE_PATH
  local "${@}"

  if [ "${RETURN_CODE}" -eq "0" ]; then
    return "0"
  fi

  stdout.log_error "Error detected! Exit code: ${RETURN_CODE}"
  stdout.log_info "Rolling back release..."
  remote.rollback SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}"
}

remote.rollback_or_exit() {
  local SSH_KEY_FILE TARGET_LIST_STRING DEPLOYMENT_BASE_PATH
  local "${@}"

  remote.rollback SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}"
  error.exit_if_error $?
}
