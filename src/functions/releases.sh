#!/usr/bin/env bash

get_releases_base_path() {
  local DEPLOYMENT_BASE_PATH=${1}

  echo "${DEPLOYMENT_BASE_PATH}/releases"
}

get_new_release_path() {
  local DEPLOYMENT_BASE_PATH=${1}

  RELEASE_NAME=$(date '+%Y%m%d%H%M%S')
  RELEASES_BASE_PATH=$(get_releases_base_path "${DEPLOYMENT_BASE_PATH}")

  echo "${RELEASES_BASE_PATH}/${RELEASE_NAME}"
}

get_current_release_link() {
  local DEPLOYMENT_BASE_PATH=${1}

  echo "${DEPLOYMENT_BASE_PATH}/current"
}

get_previous_release_link() {
  local DEPLOYMENT_BASE_PATH=${1}

  echo "${DEPLOYMENT_BASE_PATH}/previous"
}

get_releases_list() {
  local RELEASES_BASE_PATH
  local "${@}"
  local RETURN_ARRAY

  RELEASES_OLD_TO_NEW=()
  RELEASES=()

  readarray -t RELEASES_OLD_TO_NEW < <(ls "${RELEASES_BASE_PATH}" | grep "\S"  | sort -t '\0' -n)

  array.reverse RELEASES_OLD_TO_NEW RELEASES

  # return the array
  RETURN_ARRAY="$( declare -p RELEASES )"
  local IFS=$'\v';
  echo "${RETURN_ARRAY#*=}"
  # restore IFS to default value: <space><tab><newline>
  IFS=' '$'\t'$'\n';
}
