#!/usr/bin/env bash

copy_current_to_new_release_or_exit() {
  local NEW_RELEASE_PATH CURRENT_RELEASE_LINK RETURN_CODE=0
  local "${@}"

  mkdir -p "${NEW_RELEASE_PATH}"
  if [ -e "${CURRENT_RELEASE_LINK}" ]; then
    cp -rf "${CURRENT_RELEASE_LINK}/" "${NEW_RELEASE_PATH}"
    RETURN_CODE=$?
  fi
  error.exit_if_error ${RETURN_CODE}
}

remote.copy_current_to_new_release() {
  local SSH_KEY_FILE TARGET_LIST_STRING NEW_RELEASE_PATH CURRENT_RELEASE_LINK ARTIFACT_OUT_PATH
  local "${@}"

  parallel.ssh SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    LOG_FILE="${ARTIFACT_OUT_PATH}/${FUNCNAME[0]}" \
    SCRIPT="copy_current_to_new_release_or_exit NEW_RELEASE_PATH='${NEW_RELEASE_PATH}' CURRENT_RELEASE_LINK='${CURRENT_RELEASE_LINK}'"

  return $?
}

remote.update_new_release() {
  local SSH_KEY_FILE \
    TARGET_LIST_STRING \
    NEW_RELEASE_PATH \
    SOURCE_PATH \
    ARTIFACT_OUT_PATH \
    DELETE_EXTRANEOUS_FILES_FROM_DESTINATION='true' \
    EXCLUSION_PATHS="'.git' 'node_modules'"
  local "${@}"

  OPTIONS="--recursive --links --times --update --delete --checksum"
  if [ "${DELETE_EXTRANEOUS_FILES_FROM_DESTINATION}" != "true" ]; then
    OPTIONS="--recursive --links --times --update --checksum"
  fi

  parallel.rsync SSH_KEY_FILE="${SSH_KEY_FILE}" \
    SOURCE_PATH="${SOURCE_PATH}" \
    DESTINATION_LIST_STRING="${TARGET_LIST_STRING}" \
    DESTINATION_BASE_PATH="${NEW_RELEASE_PATH}" \
    LOG_FILE="${ARTIFACT_OUT_PATH}/${FUNCNAME[0]}" \
    OPTIONS="${OPTIONS}" \
    EXCLUSION_PATHS="${EXCLUSION_PATHS}"

  return $?
}

update_release_link() {
  local NEW_RELEASE_PATH CURRENT_RELEASE_LINK
  local "${@}"

  cp -PTf "${CURRENT_RELEASE_LINK}" "${PREVIOUS_RELEASE_LINK}" # Update the 'previous' release link
  CURRENT_RELEASE_LINK_TMP="${CURRENT_RELEASE_LINK}.tmp"
  ln -s "$(realpath --relative-to="$(dirname "${CURRENT_RELEASE_LINK_TMP}")" "${NEW_RELEASE_PATH}")" "${CURRENT_RELEASE_LINK_TMP}"
  # 'mv' is an atomic operation, while 'ln -sf' is not. @see https://gist.github.com/hussfelt/3931118#motivation
  # --no-target-directory Treats the target as a file even if it's a directory so that, when the target is a symlink,
  #                       it replaces the symlink as opposed to the contents of the folder pointed to by the symlink
  # --force Overwrites without prompting
  mv --no-target-directory --force "${CURRENT_RELEASE_LINK_TMP}" "${CURRENT_RELEASE_LINK}"

  return $?
}

update_release_link_or_exit() {
  local NEW_RELEASE_PATH CURRENT_RELEASE_LINK PREVIOUS_RELEASE_LINK
  local "${@}"

  stdout.log_info "Update release link..."
  update_release_link NEW_RELEASE_PATH="${NEW_RELEASE_PATH}" \
    CURRENT_RELEASE_LINK="${CURRENT_RELEASE_LINK}" \
    PREVIOUS_RELEASE_LINK="${PREVIOUS_RELEASE_LINK}"
  error.exit_if_error $?
  stdout.log_info "Done!"
}

remote.update_release_link() {
  local SSH_KEY_FILE TARGET_LIST_STRING NEW_RELEASE_PATH CURRENT_RELEASE_LINK PREVIOUS_RELEASE_LINK ARTIFACT_OUT_PATH
  local "${@}"

  parallel.ssh SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    LOG_FILE="${ARTIFACT_OUT_PATH}/${FUNCNAME[0]}" \
    SCRIPT="update_release_link_or_exit NEW_RELEASE_PATH='${NEW_RELEASE_PATH}' CURRENT_RELEASE_LINK='${CURRENT_RELEASE_LINK}' PREVIOUS_RELEASE_LINK='${PREVIOUS_RELEASE_LINK}'"

  return $?
}

remote.deploy_or_exit() {
  local SSH_KEY_FILE \
    TARGET_LIST_STRING='' \
    DEPLOYMENT_BASE_PATH \
    SOURCE_PATH='./' \
    RELEASES_TO_KEEP=3 \
    DELETE_EXTRANEOUS_FILES_FROM_DESTINATION='true' \
    EXCLUSION_PATHS="'.git' 'node_modules'"
  local "${@}"

  RELEASES_BASE_PATH=$(get_releases_base_path "${DEPLOYMENT_BASE_PATH}")
  NEW_RELEASE_PATH=$(get_new_release_path "${DEPLOYMENT_BASE_PATH}")
  CURRENT_RELEASE_LINK=$(get_current_release_link "${DEPLOYMENT_BASE_PATH}")
  PREVIOUS_RELEASE_LINK=$(get_previous_release_link "${DEPLOYMENT_BASE_PATH}")

  if [ -d "${NEW_RELEASE_PATH}" ]; then
    stdout.log_error "A release already exists in the path '${NEW_RELEASE_PATH}'. Please try again."
    exit 1
  fi

  stdout.log_info "Deploying on '${TARGET_LIST_STRING}' '${NEW_RELEASE_PATH}' '${CURRENT_RELEASE_LINK}' '${PREVIOUS_RELEASE_LINK}'..."

  if runtime.function_exists "before_transfer"; then
    before_transfer
    RETURN_CODE=$?
    error.exit_if_error $RETURN_CODE
  fi

  stdout.log_info "Copying last release into new release ( ${CURRENT_RELEASE_LINK} => ${NEW_RELEASE_PATH} ) ..."
  remote.copy_current_to_new_release SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    NEW_RELEASE_PATH="${NEW_RELEASE_PATH}" \
    CURRENT_RELEASE_LINK="${CURRENT_RELEASE_LINK}" \
    ARTIFACT_OUT_PATH="${ARTIFACT_OUT_PATH}"
  RETURN_CODE=$?
  remote.remove_release_if_error RETURN_CODE=${RETURN_CODE} \
    SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    RELEASE_PATH="${NEW_RELEASE_PATH}"
  error.exit_if_error $RETURN_CODE
  stdout.log_info "Done!"

  stdout.log_info "Update new release..."
  remote.update_new_release SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    NEW_RELEASE_PATH="${NEW_RELEASE_PATH}" \
    SOURCE_PATH="${SOURCE_PATH}" \
    ARTIFACT_OUT_PATH="${ARTIFACT_OUT_PATH}" \
    DELETE_EXTRANEOUS_FILES_FROM_DESTINATION="${DELETE_EXTRANEOUS_FILES_FROM_DESTINATION}" \
    EXCLUSION_PATHS="${EXCLUSION_PATHS}"
  RETURN_CODE=$?
  remote.remove_release_if_error RETURN_CODE=${RETURN_CODE} \
    SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    RELEASE_PATH="${NEW_RELEASE_PATH}"
  error.exit_if_error $RETURN_CODE
  stdout.log_info "Done!"

  if runtime.function_exists "before_release_link_update"; then
    remote.before_release_link_update NEW_RELEASE_PATH="${NEW_RELEASE_PATH}"
    RETURN_CODE=$?
    remote.remove_release_if_error RETURN_CODE=${RETURN_CODE} \
      SSH_KEY_FILE="${SSH_KEY_FILE}" \
      TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
      RELEASE_PATH="${NEW_RELEASE_PATH}"
    error.exit_if_error $RETURN_CODE
    stdout.log_info "Done!"
  fi

  stdout.log_info "Updating current release symlink and finalizing deployment..."
  remote.update_release_link SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    NEW_RELEASE_PATH="${NEW_RELEASE_PATH}" \
    CURRENT_RELEASE_LINK="${CURRENT_RELEASE_LINK}" \
    PREVIOUS_RELEASE_LINK="${PREVIOUS_RELEASE_LINK}" \
    ARTIFACT_OUT_PATH="${ARTIFACT_OUT_PATH}"
  RETURN_CODE=$?
  remote.rollback_if_error RETURN_CODE=${RETURN_CODE} \
    SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}"
  error.exit_if_error $RETURN_CODE
  stdout.log_info "Done!"

  if runtime.function_exists "after_release_link_update_on_all_servers"; then
    remote.after_release_link_update_on_all_servers SSH_KEY_FILE="${SSH_KEY_FILE}" \
      TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
      NEW_RELEASE_PATH="${NEW_RELEASE_PATH}"
    RETURN_CODE=$?
    remote.rollback_if_error RETURN_CODE=${RETURN_CODE} \
      SSH_KEY_FILE="${SSH_KEY_FILE}" \
      TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
      DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}"
    error.exit_if_error $RETURN_CODE
    stdout.log_info "Done!"
  fi

  if runtime.function_exists "after_release_link_update_on_one_server"; then
    remote.after_release_link_update_on_one_server SSH_KEY_FILE="${SSH_KEY_FILE}" \
      TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
      NEW_RELEASE_PATH="${NEW_RELEASE_PATH}"
    RETURN_CODE=$?
    remote.rollback_if_error RETURN_CODE=${RETURN_CODE} \
      SSH_KEY_FILE="${SSH_KEY_FILE}" \
      TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
      DEPLOYMENT_BASE_PATH="${DEPLOYMENT_BASE_PATH}"
    error.exit_if_error $RETURN_CODE
    stdout.log_info "Done!"
  fi

  stdout.log_info "Removing outdated releases..."
  remote.remove_outdated_releases SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    RELEASES_BASE_PATH="${RELEASES_BASE_PATH}" \
    RELEASES_TO_KEEP="${RELEASES_TO_KEEP}"
  error.exit_if_error $?
  stdout.log_info "Done!"
}

remote.before_release_link_update() {
  local SSH_KEY_FILE TARGET_LIST_STRING NEW_RELEASE_PATH
  local "${@}"

  parallel.ssh SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    LOG_FILE="${ARTIFACT_OUT_PATH}/${FUNCNAME[0]}" \
    SCRIPT="before_release_link_update NEW_RELEASE_PATH='${NEW_RELEASE_PATH}'"

  RETURN_CODE=$?
  if error.is_error $RETURN_CODE; then
    return ${RETURN_CODE}
  fi
}

remote.after_release_link_update_on_all_servers() {
  local SSH_KEY_FILE TARGET_LIST_STRING NEW_RELEASE_PATH
  local "${@}"

  parallel.ssh SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    LOG_FILE="${ARTIFACT_OUT_PATH}/${FUNCNAME[0]}" \
    SCRIPT="after_release_link_update_on_all_servers NEW_RELEASE_PATH='${NEW_RELEASE_PATH}'"

  RETURN_CODE=$?
  if error.is_error $RETURN_CODE; then
    return ${RETURN_CODE}
  fi
}

remote.after_release_link_update_on_one_server() {
  local SSH_KEY_FILE TARGET_LIST_STRING NEW_RELEASE_PATH
  local "${@}"

  parallel.ssh SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    LOG_FILE="${ARTIFACT_OUT_PATH}/${FUNCNAME[0]}" \
    SCRIPT="after_release_link_update_on_one_server NEW_RELEASE_PATH='${NEW_RELEASE_PATH}'"

  RETURN_CODE=$?
  if error.is_error $RETURN_CODE; then
    return ${RETURN_CODE}
  fi
}
