#!/usr/bin/env bash

# This is to be used when there are errors in deployment
remove_release() {
  local RELEASE_PATH
  local "${@}"

  rm -rf "${RELEASE_PATH}"
}

# This is to be used when there are errors in deployment, BEFORE updating the current release link
# in which case we only need to remove the aborted release folder
remote.remove_release_if_error() {
  local RETURN_CODE="0" SSH_KEY_FILE TARGET_LIST_STRING RELEASE_PATH
  local "${@}"

  if [ "${RETURN_CODE}" -eq "0" ]; then
    return "0"
  fi

  stdout.log_error "Error detected! Exit code: ${RETURN_CODE}"
  stdout.log_info "Removing aborted release '$(basename "${RELEASE_PATH}")'..."

  parallel.ssh SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    LOG_FILE="${ARTIFACT_OUT_PATH}/${FUNCNAME[0]}" \
    SCRIPT="remove_release RELEASE_PATH='${RELEASE_PATH}'"

  error.exit_if_error $?
}

remove_outdated_releases() {
  local RELEASES_BASE_PATH RELEASES_TO_KEEP=3
  local "${@}"

  local RELEASES
  eval declare -a RELEASES="$( get_releases_list RELEASES_BASE_PATH="${RELEASES_BASE_PATH}" )"
  stdout.log_debug "RELEASES=${RELEASES[*]}"

  LENGTH=${#RELEASES[@]}

  if [ "${LENGTH}" -le ${RELEASES_TO_KEEP} ]; then
    stdout.log_info "No outdated releases to remove."
    return 0
  fi

  for (( i = RELEASES_TO_KEEP; i < LENGTH; i++ )); do
    RELEASE_PATH="${RELEASES_BASE_PATH}/${RELEASES[$i]}"
    stdout.log_info "Removing release '${RELEASE_PATH}' ..."
    remove_release RELEASE_PATH="${RELEASE_PATH}"
    stdout.log_info "Done."
  done
}

remote.remove_outdated_releases() {
  local SSH_KEY_FILE TARGET_LIST_STRING RELEASES_BASE_PATH RELEASES_TO_KEEP
  local "${@}"

  parallel.ssh SSH_KEY_FILE="${SSH_KEY_FILE}" \
    TARGET_LIST_STRING="${TARGET_LIST_STRING}" \
    LOG_FILE="${ARTIFACT_OUT_PATH}/${FUNCNAME[0]}" \
    SCRIPT="remove_outdated_releases RELEASES_BASE_PATH='${RELEASES_BASE_PATH}' RELEASES_TO_KEEP='${RELEASES_TO_KEEP}'"

  return $?
}
