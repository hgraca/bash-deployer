#!/usr/bin/env bash

# shellcheck source-path=SCRIPTDIR
DIR_bash_deployer_bin=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
DIR_ROOT="${DIR_bash_deployer_bin}/.."

. "${DIR_ROOT}/src/main.sh"
